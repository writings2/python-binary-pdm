import polars as pl

df = pl.DataFrame(
    {
        'Company': ['Ford','Toyota','Toyota','Honda','Toyota',
                    'Ford','Honda','Subaru','Ford','Subaru'],
        'Model': ['F-Series','RAV4','Camry','CR-V','Tacoma',
                  'Explorer','Accord','CrossTrek','Escape','Outback'],
        'Sales': [58283,32390,25500,18081,21837,19076,11619,15126,
                  13272,10928]
        }
)

print(df)